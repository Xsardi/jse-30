package ru.t1.tbobkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "logout";

    @NotNull
    private static final String DESCRIPTION = "logout current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
