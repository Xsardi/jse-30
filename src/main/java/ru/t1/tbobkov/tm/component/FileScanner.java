package ru.t1.tbobkov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class FileScanner extends Thread {

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    @Nullable
    private Bootstrap bootstrap;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            for (@NotNull final File file : folder.listFiles()) {
                if (file.isDirectory()) continue;
                @NotNull final String fileName = file.getName();
                final boolean check = commands.contains(fileName);
                if (check) {
                    try {
                        file.delete();
                        bootstrap.processCommand(fileName, false);
                    } catch (@NotNull final Exception e) {
                        bootstrap.getLoggerService().error(e);
                    }
                }
            }
        }
    }

}
