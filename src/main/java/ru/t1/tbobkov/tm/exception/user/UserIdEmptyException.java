package ru.t1.tbobkov.tm.exception.user;

public final class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("Error! User id is empty...");
    }

}
